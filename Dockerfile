FROM php:7.4.11-alpine as CODE_BASE

# First, create the application directory, and some auxilary directories for scripts and such
RUN mkdir -p /opt/apps/operator-api /opt/apps/operator-api/bin

# Next, set our working directory
WORKDIR /opt/apps/operator-api

# Copy in the Composer installation script
COPY bin/install-composer.sh bin

# Install Composer
RUN sh ./bin/install-composer.sh

## We need to create a composer group and user, and create a home directory for it, so we keep the rest of our image safe
RUN addgroup -S composer \
    && adduser -S composer -G composer \
    && chown -R composer /opt/apps/operator-api

# Next we want to switch over to the composer user
USER composer

# Copy in our dependency files
COPY --chown=composer composer.json composer.lock ./

# Install all the dependencies without running any installation scripts
RUN php composer.phar install --no-dev --no-scripts --no-autoloader --prefer-dist

# Copy in our actual source code
COPY --chown=composer . .

# Install all the dependencies running installation scripts as well
RUN php composer.phar install --no-dev --prefer-dist

FROM nginx:1.19-alpine as WEBSERVER

# Set the working directory, same as previously
WORKDIR /opt/apps/operator-api

COPY ./docker/nginx.conf /etc/nginx/nginx.conf

## We need to create a composer group and user, and create a home directory for it, so we keep the rest of our image safe
RUN adduser -S www-data -G www-data

# Copy in ONLY the public directory of our project.
# This is where all the static assets will live, which nginx will serve for us.
# Any PHP requests will be passed down to FPM
COPY --from=CODE_BASE --chown=www-data /opt/apps/operator-api/public /opt/apps/operator-api/public

CMD ["nginx", "-c", "/etc/nginx/nginx.conf"]

FROM php:7.3-fpm-alpine as FPM_SERVER

# Set the working directory, same as previously
WORKDIR /opt/apps/operator-api

# We need to install the PHP mysql extension for our application
RUN docker-php-ext-install pdo pdo_mysql

# Copy in ONLY the public directory of our project.
# This is where all the static assets will live, which nginx will serve for us.
# Any PHP requests will be passed down to FPM
COPY --from=CODE_BASE --chown=www-data /opt/apps/operator-api /opt/apps/operator-api

FROM FPM_SERVER as TESTING

# Next we want to switch over to the composer user
USER root

# Next, copy in the codebase to ensure all testing scripts are there
COPY --from=CODE_BASE --chown=www-data /opt/apps/operator-api /opt/apps/operator-api

# Install all the dependencies without running any installation scripts
RUN php composer.phar install --prefer-dist

CMD ["php", "artisan", "test"]

FROM FPM_SERVER
